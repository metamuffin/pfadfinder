all: pfadfinder-client/assets/leaflet.css pfadfinder-client/assets/leaflet.js pfadfinder-client/assets/geojson.d.ts pfadfinder-client/assets/leaflet.d.ts

# todo this is not perfect
pfadfinder-client/assets/leaflet.css:
	mkdir -p pfadfinder-client/assets
	curl https://unpkg.com/leaflet@1.9.3/dist/leaflet.css > $@
pfadfinder-client/assets/leaflet.js:
	mkdir -p pfadfinder-client/assets
	curl https://unpkg.com/leaflet@1.9.3/dist/leaflet.js > $@
pfadfinder-client/assets/geojson.d.ts:
	mkdir -p pfadfinder-client/assets
	curl https://raw.githubusercontent.com/DefinitelyTyped/DefinitelyTyped/master/types/geojson/index.d.ts > $@
pfadfinder-client/assets/leaflet.d.ts:
	mkdir -p pfadfinder-client/assets
	curl https://raw.githubusercontent.com/DefinitelyTyped/DefinitelyTyped/master/types/leaflet/index.d.ts | sed "s/from 'geojson/from '.\\/geojson.d.ts/" > $@
