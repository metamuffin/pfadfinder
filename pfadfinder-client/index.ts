/*
    This file is part of pfadfinder (https://codeberg.org/metamuffin/pfadfinder)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin@disroot.org>
*/
/// <reference lib="dom" />

import { CircleMarker, LeafletMouseEvent, Polyline } from "./assets/leaflet.d.ts";
import { ebutton, eli, eol, ep } from "./helper.ts";
import { cnv, Route, Waypoint, ProgressEvent } from "./types.ts";


interface RoutingPoint {
    point?: Waypoint,
    marker?: CircleMarker
    display_el?: HTMLElement
}

const start: RoutingPoint = {};
const end: RoutingPoint = {};
let markers: CircleMarker[] = []
let instruction_markers: CircleMarker[] = []
let route: Polyline | undefined
let map: L.Map;

let stats_el: HTMLElement
let instructions_el: HTMLElement;
const h_scale = 0.5

globalThis.addEventListener("load", () => {
    map = window.L.map('map').setView([48.890, 10.767], 8);
    window.L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    const o = document.getElementById("overlay")!
    o.append(
        ep("From", { class: "fromto" }),
        start.display_el = ep("(not set)", { class: "locname" }),
        ep("To", { class: "fromto" }),
        end.display_el = ep("(not set)", { class: "locname" }),
        ebutton("Plan route", { onclick: () => start_planning() }),
        ep("Stats"),
        stats_el = ep("", { class: "stats" }),
        instructions_el = eol({ class: "instructions" }),
    )

    async function set_point(point: RoutingPoint, ev: LeafletMouseEvent) {
        const res = await fetch(`/query_location?lat=${ev.latlng.lat}&lon=${ev.latlng.lng}`)
        const r: Waypoint | null = await res.json()
        console.log(r);
        if (!r) return
        point.point = r;
        point.marker?.removeFrom(map)
        point.marker = new window.L.CircleMarker(cnv(r), { color: "red" }).addTo(map)
        point.display_el!.textContent = await get_display(point.point)
    }

    map.on("click", ev => {
        set_point(start, ev)
    })
    map.on("contextmenu", ev => {
        set_point(end, ev)
    })
})

function start_planning() {
    if (!start.point?.id || !end.point?.id) return

    const source = new EventSource(`/path_progress?start=${start.point.id}&end=${end.point?.id}&h_scale=${h_scale}`)
    source.onmessage = m => {
        const r: Route | ProgressEvent = JSON.parse(m.data)
        markers.forEach(e => e.removeFrom(map))
        instruction_markers.forEach(e => e.removeFrom(map))
        instructions_el.innerHTML = ""

        if ("waypoints" in r) {
            source.close()
            route?.removeFrom(map)
            route = window.L.polyline(r.waypoints.map(cnv), { color: "red" }).addTo(map)
            stats_el.textContent = `distance=${format_distnace(r.description.distance)}\n`
            stats_el.textContent += `duration=${format_duration(r.description.duration)}\n`
            instruction_markers = r.description.instructions.map(e =>
                window.L.circleMarker(cnv(r.waypoints[e.index]), { radius: 8, color: "#ff7777" })
                    .addTo(map)
                    .bindTooltip(`${e.text}`)
                    .openPopup()
            )
            instructions_el.append(...r.description.instructions.map((e, i) => {
                return eli({ onclick: () => map.setView(instruction_markers[i].getLatLng(), 18) }, ep(e.text))
            }))
        } else {
            markers = r.open_sample.map(b => new window.L.CircleMarker(cnv(b)).addTo(map))
            stats_el.textContent = `heuristic_thres=${r.heuristic.toFixed(1)}\n`
            stats_el.textContent += `num_open=${r.num_open}\n`
            stats_el.textContent += `num_explored=${r.num_explored}\n`
        }
    }
}

async function get_display(w: Waypoint) {
    const res = await fetch(`https://nominatim.openstreetmap.org/reverse?lat=${w.lat}&lon=${w.lon}&format=json`)
    const r = await res.json()
    return r.display_name ?? "unknown"
}

function format_duration(v: number) {
    if (v > 60 * 60 * 24) return (v / 60 / 60 / 24).toFixed(1) + "d"
    if (v > 60 * 60) return (v / 60 / 60).toFixed(1) + "h"
    if (v > 60) return (v / 60).toFixed(1) + "min"
    return v.toFixed(1) + "s"
}
function format_distnace(v: number) {
    if (v > 1000) return (v / 1000).toFixed(1) + "km"
    return v.toFixed(1) + "m"
}