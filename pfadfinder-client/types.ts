import { LatLngExpression } from "./assets/leaflet.d.ts";

export interface Waypoint {
    lat: number,
    lon: number,
    id: number,
}
export interface Route {
    waypoints: Waypoint[],
    description: RouteDescription,
}
export interface RouteDescription {
    duration: number,
    distance: number,
    instructions: Instruction[]
}
export interface Instruction {
    index: number
    text: string
}
export interface ProgressEvent {
    open_sample: Waypoint[],
    heuristic: number,
    num_open: number,
    num_explored: number,
}

export function cnv(p: Waypoint): LatLngExpression {
    return { lat: p.lat, lng: p.lon }
}

