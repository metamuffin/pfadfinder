/*
    This file is part of pfadfinder (https://codeberg.org/metamuffin/pfadfinder)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin@disroot.org>
*/
pub mod misc;
pub mod pathfinder;

use clap::Parser;
use misc::r_query_location;
use pathfinder::{r_path, r_path_progress};
use pfadfinder::{database::Database, import::import, Pfadfinder};
use rocket::{
    fairing::AdHoc,
    fs::FileServer,
    get,
    http::{ContentType, Header},
    routes,
    serde::Serialize,
};
use std::{path::PathBuf, str::FromStr};
use tokio::fs::File;

#[derive(Debug, Parser)]
struct Args {
    #[arg(short = 'd', long)]
    database: Option<PathBuf>,
    #[arg(short = 'm', long)]
    map_source: Vec<PathBuf>,
}

#[tokio::main]
async fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .parse_env("LOG")
        .init();

    let args = Args::parse();
    let mut database = Database::new(
        &args
            .database
            .unwrap_or(PathBuf::from_str("data/db").unwrap()),
    );

    for map_source in args.map_source {
        import(&mut database, &map_source)
    }

    let pf = Pfadfinder::new(database);

    let _ = rocket::build()
        .manage(pf)
        .attach(AdHoc::on_response("set server header", |_req, res| {
            res.set_header(Header::new("server", "pfadfinder-server"));
            Box::pin(async {})
        }))
        .mount("/assets", FileServer::from("pfadfinder-client/assets"))
        .mount(
            "/",
            routes![r_index, r_path, r_path_progress, r_query_location],
        )
        .launch()
        .await
        .unwrap();
}

#[get("/")]
async fn r_index() -> (ContentType, File) {
    (
        ContentType::HTML,
        File::open("pfadfinder-client/index.html").await.unwrap(),
    )
}

#[derive(Clone, Serialize)]
pub struct Waypoint {
    lat: f64,
    lon: f64,
    id: u64,
}

impl Waypoint {
    pub fn from_id(pf: &Pfadfinder, id: u64) -> Self {
        let loc = pf.database.location(id);
        Waypoint {
            lat: loc.lat_deg(),
            lon: loc.lon_deg(),
            id,
        }
    }
}
