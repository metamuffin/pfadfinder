/*
    This file is part of pfadfinder (https://codeberg.org/metamuffin/pfadfinder)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin@disroot.org>
*/
use crate::Waypoint;
use pfadfinder::{Location, Pfadfinder};
use rocket::{get, serde::json::Json, State};
use std::sync::Arc;

#[get("/query_location?<lat>&<lon>")]
pub fn r_query_location(pf: &State<Arc<Pfadfinder>>, lat: f64, lon: f64) -> Json<Option<Waypoint>> {
    let search_loc = Location::from_degree(lat, lon);

    let id = pf
        .database
        .nodes_spacial
        .range((search_loc.chunk(), u64::MIN)..(search_loc.chunk(), u64::MAX))
        .map(|((_, i), _)| {
            let loc = pf.database.location(i);
            ((Location::distance(loc, search_loc) * 1000.0) as i64, i)
        })
        .min()
        .map(|(_, i)| i);

    Json(id.map(|id| Waypoint::from_id(&pf, id)))
}
