/*
    This file is part of pfadfinder (https://codeberg.org/metamuffin/pfadfinder)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin@disroot.org>
*/
use crate::Waypoint;
use pfadfinder::{description::RouteDescription, Pfadfinder};
use rocket::{
    async_stream::stream,
    get,
    response::stream::{Event, EventStream},
    serde::json::Json,
    State,
};
use serde::Serialize;
use std::{ops::Deref, sync::Arc, time::Duration};
use tokio::{sync::mpsc::unbounded_channel, time::timeout};

#[derive(Clone, Serialize)]
pub struct Route {
    waypoints: Vec<Waypoint>,
    description: RouteDescription,
}

#[derive(Clone, Serialize)]
pub struct Progress {
    pub open_sample: Vec<Waypoint>,
    pub heuristic: f64,
    pub num_open: usize,
    pub num_explored: usize,
}

#[get("/path?<start>&<end>&<h_scale>")]
pub fn r_path(
    pf: &State<Arc<Pfadfinder>>,
    start: u64,
    end: u64,
    h_scale: Option<f64>,
) -> Json<Route> {
    let h_scale = h_scale.unwrap_or(0.5).clamp(0.05, 1000.0);
    let path = pf.find_path(start, end, h_scale, None);
    let description = pf.describe_path(&path);
    Json(Route {
        waypoints: path
            .into_iter()
            .map(|id| Waypoint::from_id(&pf, id))
            .collect(),
        description,
    })
}

#[get("/path_progress?<start>&<end>&<h_scale>")]
pub fn r_path_progress(
    pf: &State<Arc<Pfadfinder>>,
    start: u64,
    end: u64,
    h_scale: Option<f64>,
) -> EventStream![] {
    let pf = pf.deref().clone();
    let h_scale = h_scale.unwrap_or(0.5).clamp(0.05, 1000.0);

    let (progress_tx, mut progress_rx) = unbounded_channel();

    let pf1 = pf.clone();
    let task =
        tokio::task::spawn_blocking(move || pf1.find_path(start, end, h_scale, Some(progress_tx)));

    stream! {
        loop {
            if task.is_finished() {
                let path = task.await.unwrap();
                let description = pf.describe_path(&path);
                yield Event::json(&Route {
                    waypoints: path
                        .into_iter()
                        .map(|id| Waypoint::from_id(&pf,id))
                        .collect(),
                    description,
                });
                break;
            }
            if let Ok(event) = timeout(Duration::from_millis(50), progress_rx.recv()).await {
                if let Some(event) = event {
                    yield Event::json(&Progress {
                        open_sample: event
                            .open_sample
                            .into_iter()
                            .map(|e| Waypoint::from_id(&pf, e))
                            .collect(),
                        heuristic: event.heuristic,
                        num_open: event.num_open,
                        num_explored: event.num_explored,
                    });
                }
            }

        }
    }
    .into()
}
