/*
    This file is part of pfadfinder (https://codeberg.org/metamuffin/pfadfinder)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin@disroot.org>
*/
use crate::{Location, Pfadfinder};
use chashmap::CHashMap;
use log::info;
use std::{
    collections::BinaryHeap,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, Mutex,
    },
    thread,
    time::{Duration, Instant},
};
use tokio::sync::mpsc::UnboundedSender;

#[derive(Debug, Clone, PartialEq, PartialOrd)]
struct Attrib {
    heuristic: f64,
    time_taken: f64,
    id: u64,
    location: Location,
}

impl Eq for Attrib {}
impl Ord for Attrib {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

#[derive(Debug, Clone)]
pub struct ProgressEvent {
    pub open_sample: Vec<u64>,
    pub heuristic: f64,
    pub num_open: usize,
    pub num_explored: usize,
}

impl Pfadfinder {
    pub fn find_path(
        self: &Arc<Self>,
        start: u64,
        end: u64,
        h_scale: f64,
        mut progress: Option<UnboundedSender<ProgressEvent>>,
    ) -> Vec<u64> {
        let end_location = self.database.location(end);
        let start_location = self.database.location(start);

        let queue = Arc::new(Mutex::new(BinaryHeap::new()));
        let explored = Arc::new(CHashMap::new());
        let finished = Arc::new(AtomicBool::new(false));

        queue.lock().unwrap().push(Attrib {
            heuristic: Location::distance(self.database.location(start), end_location),
            id: start,
            location: start_location,
            time_taken: 0.0,
        });
        explored.insert(start, None);

        info!("pathfinding started");
        let ts_start = Instant::now();

        for _ in 0..thread::available_parallelism().unwrap().into() {
            let queue = queue.clone();
            let explored = explored.clone();
            let this = self.clone();
            let finished = finished.clone();
            thread::spawn(move || {
                while !finished.load(Ordering::Relaxed) {
                    let next = queue.lock().unwrap().pop();
                    if let Some(attrib) = next {
                        for (id, eattrib) in this.database.connections(attrib.id) {
                            // if !eattrib.kind.supports_car() {
                            //     continue;
                            // }
                            let speed = eattrib.speed();
                            if speed == 0.0 {
                                continue;
                            }
                            let location = this.database.location(id);
                            let distance = Location::distance(attrib.location, location);
                            let segment_duration = distance / speed;
                            // debug!("{distance}m with {speed}m/s in {segment_duration}s");
                            let time_taken = attrib.time_taken + segment_duration;

                            if let Some(mut k) = explored.get_mut(&id) {
                                if let Some((from, tt)) = k.as_mut() {
                                    // in case we found a better route to an already explored node
                                    // (happens because of threading)
                                    if *tt > time_taken {
                                        *from = attrib.id;
                                        *tt = time_taken;
                                    }
                                }
                                continue;
                            } else {
                                explored.insert(id, Some((attrib.id, time_taken)));
                            }

                            if id == end {
                                finished.store(true, Ordering::Relaxed);
                            }
                            queue.lock().unwrap().push(Attrib {
                                location,
                                heuristic: -(time_taken
                                    + Location::distance(location, end_location) / speed * h_scale),
                                time_taken,
                                id,
                            });
                        }
                    } else {
                        thread::sleep(Duration::from_millis(10));
                    }
                }
            });
        }

        for i in 0.. {
            if finished.load(Ordering::Relaxed) {
                break;
            }
            if i % 50 == 0 {
                let queue = queue.lock().unwrap();
                let open_sample = queue
                    .iter()
                    .take(512)
                    .map(|e| e.to_owned())
                    .collect::<Vec<_>>();
                let num_open = queue.len();
                drop(queue);

                let heuristic = open_sample.get(0).map(|e| e.heuristic).unwrap_or(0.0);
                if let Some(progress) = &mut progress {
                    progress
                        .send(ProgressEvent {
                            open_sample: open_sample.into_iter().map(|e| e.id).collect(),
                            heuristic,
                            num_open,
                            num_explored: explored.len(),
                        })
                        .unwrap();
                }
                info!("num_open={} heuristic={}", num_open, heuristic);
            }
            thread::sleep(Duration::from_millis(5));
        }

        let ts_end = Instant::now();
        info!("pathfinding finished in {:?}", ts_end - ts_start);

        let mut path = vec![end];
        while let Some((prev, _)) = explored.get(path.last().unwrap()).unwrap().clone() {
            path.push(prev);
        }
        path.reverse();

        path
    }
}
