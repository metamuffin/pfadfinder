/*
    This file is part of pfadfinder (https://codeberg.org/metamuffin/pfadfinder)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin@disroot.org>
*/
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct WayAttrib {
    pub name: Option<String>,
    pub speed: Option<f64>,
    pub kind: WayKind,
    pub roundabout: bool,
    pub link: bool,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub enum WayKind {
    Motorway,
    Trunk,
    Primary,
    Secondary,
    Tertiary,
    Unclassified,
    Residential,
    Service,
    Path,
    Cycleway,
    Track,
    Unknown,
    Footway,
}

impl Default for WayKind {
    fn default() -> Self {
        Self::Unknown
    }
}

impl WayAttrib {
    pub fn name(&self) -> String {
        self.name.clone().unwrap_or(format!(
            "a nameless {}{}",
            self.kind.name(),
            if self.link { " link" } else { "" }
        ))
    }
}

impl WayKind {
    pub fn speed(&self) -> f64 {
        (match self {
            WayKind::Motorway => 150.,
            WayKind::Trunk => 60., // technically 100 but practially less
            WayKind::Primary => 50.,
            WayKind::Secondary => 50.,
            WayKind::Tertiary => 50.,
            WayKind::Unclassified => 30.,
            WayKind::Residential => 30.,
            WayKind::Cycleway => 20.,
            WayKind::Service => 10.,
            WayKind::Track => 15.,
            WayKind::Path => 10.,
            WayKind::Footway => 5.,
            WayKind::Unknown => 5.,
        }) / 3.6
    }
    pub fn supports_car(&self) -> bool {
        match self {
            WayKind::Motorway
            | WayKind::Trunk
            | WayKind::Primary
            | WayKind::Secondary
            | WayKind::Tertiary
            | WayKind::Unclassified
            | WayKind::Residential
            | WayKind::Path
            | WayKind::Service => true,
            WayKind::Cycleway | WayKind::Footway | WayKind::Track | WayKind::Unknown => false,
        }
    }
    pub fn rank(&self) -> usize {
        match self {
            WayKind::Motorway => 7,
            WayKind::Trunk => 6,
            WayKind::Primary => 5,
            WayKind::Secondary => 4,
            WayKind::Tertiary => 3,
            WayKind::Residential => 2,
            WayKind::Service => 1,
            WayKind::Path => 1,
            WayKind::Cycleway => 1,
            WayKind::Track => 1,
            WayKind::Footway => 1,
            WayKind::Unclassified => 1,
            WayKind::Unknown => 0,
        }
    }
    pub fn name(&self) -> &'static str {
        match self {
            WayKind::Motorway => "motorway",
            WayKind::Trunk => "trunk",
            WayKind::Primary => "primary road",
            WayKind::Secondary => "secondary road",
            WayKind::Tertiary => "tertiary road",
            WayKind::Unclassified => "unclassified road",
            WayKind::Residential => "residential road",
            WayKind::Service => "service road",
            WayKind::Path => "path",
            WayKind::Cycleway => "cycleway",
            WayKind::Track => "track",
            WayKind::Unknown => "road",
            WayKind::Footway => "footway",
        }
    }
}
