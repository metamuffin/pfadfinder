/*
    This file is part of pfadfinder (https://codeberg.org/metamuffin/pfadfinder)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin@disroot.org>
*/
use crate::{data::WayAttrib, Location};
use std::path::Path;

#[cfg(feature = "database")]
pub struct Tree<K, V>(typed_sled::Tree<K, V>);

#[cfg(feature = "database")]
impl<
        K: serde::Serialize + serde::de::DeserializeOwned + std::fmt::Debug,
        V: serde::Serialize + serde::de::DeserializeOwned,
    > Tree<K, V>
{
    pub fn open(db: &sled::Db, prefix: &str) -> Tree<K, V> {
        Self(typed_sled::Tree::open(db, prefix))
    }
    pub fn get(&self, key: &K) -> Option<V> {
        self.0.get(key).unwrap()
    }
    pub fn range(&self, range: std::ops::Range<K>) -> impl Iterator<Item = (K, V)> {
        self.0.range(range).map(Result::unwrap)
    }
    pub fn insert(&mut self, key: K, value: V) {
        self.0.insert(&key, &value).unwrap();
    }
}

#[cfg(not(feature = "database"))]
#[derive(Default)]
pub struct Tree<K, V>(std::collections::BTreeMap<K, V>);

#[cfg(not(feature = "database"))]
impl<K: std::cmp::Ord + Clone, V: Clone> Tree<K, V> {
    pub fn get(&self, key: &K) -> Option<V> {
        self.0.get(key).map(|e| e.to_owned())
    }
    pub fn range(&self, range: std::ops::Range<K>) -> impl Iterator<Item = (K, V)> + '_ {
        self.0.range(range).map(|(k, v)| (k.clone(), v.clone()))
    }
    pub fn insert(&mut self, key: K, value: V) {
        self.0.insert(key, value);
    }
}

pub struct Database {
    #[cfg(feature = "database")]
    pub db: sled::Db,

    pub nodes: Tree<u64, Location>,                 // node => location
    pub edges: Tree<(u64, u64), u64>,               // (n1, n2) => way
    pub nodes_spacial: Tree<((i32, i32), u64), ()>, // (chunk, node_id) => ()
    pub ways: Tree<u64, WayAttrib>,                 // way => way attrib
}

impl Database {
    pub fn new(_path: &Path) -> Self {
        #[cfg(feature = "database")]
        {
            log::info!("loading database");
            let db = sled::Config::new()
                // .use_compression(true)
                .path(_path)
                .mode(sled::Mode::LowSpace)
                .cache_capacity(2_000_000_000)
                .open()
                .unwrap();
            log::info!("done");
            Self {
                edges: Tree::open(&db, "e"),
                nodes: Tree::open(&db, "nl"),
                nodes_spacial: Tree::open(&db, "ns"),
                ways: Tree::open(&db, "w"),
                db,
            }
        }
        #[cfg(not(feature = "database"))]
        Self {
            edges: Default::default(),
            nodes: Default::default(),
            nodes_spacial: Default::default(),
            ways: Default::default(),
        }
    }

    pub fn location(&self, node: u64) -> Location {
        self.nodes.get(&node).unwrap_or(Location { lat: 0, lon: 0 })
    }
    pub fn connections(&self, node: u64) -> impl Iterator<Item = (u64, WayAttrib)> + '_ {
        self.edges
            .range((node, u64::MIN)..(node, u64::MAX))
            .map(|((_, k), v)| (k, self.ways.get(&v).unwrap()))
    }
}

impl WayAttrib {
    #[inline]
    pub fn speed(&self) -> f64 {
        self.speed.unwrap_or(self.kind.speed())
    }
}
