/*
    This file is part of pfadfinder (https://codeberg.org/metamuffin/pfadfinder)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin@disroot.org>
*/
use crate::{data::WayAttrib, Location, Pfadfinder};
use log::debug;
use serde::{Deserialize, Serialize};
use std::f64::consts::PI;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RouteDescription {
    pub duration: f64,
    pub distance: f64,
    pub instructions: Vec<Instruction>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Instruction {
    index: usize,
    text: String,
}

impl Pfadfinder {
    pub fn describe_path(&self, path: &Vec<u64>) -> RouteDescription {
        let mut total_duration = 0.0;
        let mut total_distance = 0.0;

        let mut prev_id = 0;
        let mut prev_dir = 0.0;
        let mut prev_way = None::<WayAttrib>;
        let mut instructions = Vec::new();

        let mut roundabout_exit_counter = 0;

        for (i, (a, b)) in path.iter().zip(path.iter().skip(1)).enumerate() {
            let a_pos = self.database.location(*a);
            let b_pos = self.database.location(*b);
            let way_id = self.database.edges.get(&(*a, *b)).unwrap();
            let way = self.database.ways.get(&way_id).unwrap();
            let distance = Location::distance(a_pos, b_pos);
            let duration = distance / way.speed();
            let dir = a_pos.direction_to(b_pos);
            let turn = (dir - prev_dir + PI).rem_euclid(PI * 2.);

            total_distance += distance;
            total_duration += duration;

            let mut junction_highest_exit_rank = 0;
            let mut junction_same_rank_exits = 0;
            let mut junction_exits = 0;

            if let Some(prev_way) = prev_way {
                for (pid, conn) in self.database.connections(*a) {
                    if pid == prev_id {
                        continue;
                    }
                    if conn.kind.rank() == way.kind.rank() {
                        junction_same_rank_exits += 1;
                    }
                    junction_exits += 1;
                    junction_highest_exit_rank = junction_highest_exit_rank.max(conn.kind.rank());
                }

                let mut inst_out = |text| {
                    debug!("out: {text}");
                    instructions.push(Instruction { index: i, text });
                };

                if junction_exits > 1 {
                    if way.roundabout {
                        roundabout_exit_counter += 1;
                    } else if prev_way.roundabout {
                        inst_out(format!(
                            "Take the {} exit at the roundabout onto {}",
                            describe_enumerate(roundabout_exit_counter + 1),
                            way.name()
                        ));
                        roundabout_exit_counter = 0;
                    } else if prev_way.kind == way.kind && !prev_way.link && way.link {
                        inst_out(format!("Leave {}", prev_way.name()));
                    } else if prev_way.kind != way.kind
                        || (junction_same_rank_exits != 1 && prev_way.name != way.name)
                        || junction_highest_exit_rank != way.kind.rank()
                    {
                        inst_out(format!("{} onto {}", describe_turn(turn), way.name()));
                    }
                }
            }

            prev_dir = dir;
            prev_way = Some(way.clone());
            prev_id = *a;
        }

        instructions.push(Instruction {
            index: path.len() - 1,
            text: "Arrive at the destination".to_string(),
        });

        RouteDescription {
            distance: total_distance,
            duration: total_duration,
            instructions,
        }
    }
}

pub fn describe_turn(v: f64) -> &'static str {
    match (v * (10. / (2. * PI))).round() as i32 {
        0 => "Take a sharp left turn",
        1 | 2 | 3 => "Turn left",
        4 => "Turn slightly left",
        5 => "Continue",
        6 => "Turn slightly right",
        7 | 8 | 9 => "Turn right",
        10 => "Take a sharp right turn",
        x => unreachable!("{}", x),
    }
}
pub fn describe_enumerate(v: usize) -> String {
    match v {
        1 => "1st".to_string(),
        2 => "2nd".to_string(),
        3 => "3rd".to_string(),
        x => format!("{x}th"),
    }
}
