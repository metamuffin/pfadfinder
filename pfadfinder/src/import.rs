/*
    This file is part of pfadfinder (https://codeberg.org/metamuffin/pfadfinder)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin@disroot.org>
*/
use crate::{
    data::{WayAttrib, WayKind},
    database::Database,
    Location,
};
use indicatif::ProgressBar;
use log::{info, warn};
use osmpbfreader::{OsmObj, OsmPbfReader};
use rayon::prelude::{ParallelBridge, ParallelIterator};
use std::{
    fs::File,
    io::BufReader,
    path::Path,
    sync::{atomic::AtomicUsize, Arc, RwLock},
};

pub fn import(db: &mut Database, input: &Path) {
    let mut reader = OsmPbfReader::new(BufReader::new(File::open(input).unwrap()));
    let db = Arc::new(RwLock::new(db));
    let num_ways = AtomicUsize::new(0);
    let bar = ProgressBar::new(50_000_000);

    reader.par_iter().par_bridge().for_each(|e| {
        bar.inc(1);
        match e.unwrap() {
            OsmObj::Way(v) => {
                num_ways.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
                let w = v.id.0 as u64;
                let mut oneway = false;
                let mut kind = None;
                let mut speed = None;
                let mut name = None;
                let mut name_fallback = None;
                let mut link = false;
                let mut roundabout = false;
                for (k, v) in v.tags.iter() {
                    match k.as_str() {
                        "oneway" => oneway = v == "yes",
                        "name" => name = Some(v.as_str().to_string()),
                        "ref" => name_fallback = Some(v.as_str().to_string()),
                        "highway" => {
                            kind =
                                Some(match v.as_str() {
                                    "motorway" | "motorway_link" | "motorway_junction"
                                    | "raceway" => WayKind::Motorway,
                                    "trunk" | "trunk_link" => WayKind::Trunk,
                                    "primary" | "primary_link" => WayKind::Primary,
                                    "secondary" | "secondary_link" => WayKind::Secondary,
                                    "tertiary" | "tertiary_link" => WayKind::Tertiary,
                                    "unclassified" => WayKind::Unclassified,
                                    "residential" => WayKind::Residential,
                                    "service" => WayKind::Service,
                                    "path" | "living_street" => WayKind::Path,
                                    "cycleway" => WayKind::Cycleway,
                                    "track" => WayKind::Track,
                                    "footway" | "steps" | "pedestrian" | "corridor"
                                    | "elevator" => WayKind::Footway,
                                    "proposed" | "construction" | "services" | "bridleway"
                                    | "bus_stop" | "razed" | "platform" | "busway" | "road"
                                    | "emergency_bay" | "rest_area" | "bus_guideway"
                                    | "abandoned" | "via_ferrata" | "disused" => WayKind::Unknown,
                                    e => {
                                        warn!("unknown way kind: {e:?}");
                                        WayKind::Unknown
                                    }
                                });
                            if v.as_str().ends_with("_link") {
                                link = true;
                            }
                        }
                        "junction" => match v.as_str() {
                            "roundabout" => {
                                roundabout = true;
                                oneway = true
                            }
                            _ => {}
                        },
                        "maxspeed" => {
                            speed = match v.as_str() {
                                "none" => None,
                                "DE:urban" => Some(30),
                                "DE:rural" => Some(100),
                                "DE:living_street" | "DE:walk" => Some(5),
                                "DE:zone:30" => Some(30),
                                "walk" => Some(5),
                                "signals" => Some(30),
                                x => match x.parse::<u64>() {
                                    Ok(l) => Some(l),
                                    Err(_) => {
                                        warn!("unknown maxspeed: {:?}", v.as_str());
                                        None
                                    }
                                },
                            }
                        }
                        _ => {}
                    }
                }
                if let Some(kind) = kind {
                    let mut db = db.write().unwrap();
                    db.ways.insert(
                        w,
                        WayAttrib {
                            name: name.or(name_fallback),
                            speed: speed.map(|e| e as f64 / 3.6),
                            kind,
                            link,
                            roundabout,
                        },
                    );
                    for (a, b) in v.nodes.iter().zip(v.nodes.iter().skip(1)) {
                        db.edges.insert((a.0 as u64, b.0 as u64), w);
                        if !oneway {
                            db.edges.insert((b.0 as u64, a.0 as u64), w);
                        }
                    }
                }
            }
            _ => (),
        }
    });
    info!(
        "imported {} edges",
        num_ways.load(std::sync::atomic::Ordering::Relaxed)
    );
    drop(reader);
    let mut reader = OsmPbfReader::new(BufReader::new(File::open(input).unwrap()));

    let num_nodes = AtomicUsize::new(0);
    let num_nodes_skipped = AtomicUsize::new(0);

    let bar = ProgressBar::new(50_000_000);
    reader.par_iter().par_bridge().for_each(|e| {
        bar.inc(1);
        match e.unwrap() {
            OsmObj::Node(v) => {
                let mut skip = false;
                let n = v.id.0 as u64;
                if db
                    .read()
                    .unwrap()
                    .edges
                    .range((n, u64::MIN)..(n, u64::MAX))
                    .next()
                    .is_none()
                {
                    skip = true
                }
                if !skip {
                    num_nodes.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
                    let loc = Location {
                        lat: v.decimicro_lat,
                        lon: v.decimicro_lon,
                    };
                    let mut db = db.write().unwrap();
                    db.nodes.insert(n, loc);
                    db.nodes_spacial.insert((loc.chunk(), n), ());
                } else {
                    num_nodes_skipped.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
                }
            }
            _ => (),
        }
    });

    info!(
        "imported {} nodes ({} could be skipped)",
        num_nodes.load(std::sync::atomic::Ordering::Relaxed),
        num_nodes_skipped.load(std::sync::atomic::Ordering::Relaxed),
    )
}
