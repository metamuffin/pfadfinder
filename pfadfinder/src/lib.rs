/*
    This file is part of pfadfinder (https://codeberg.org/metamuffin/pfadfinder)
    which is licensed under the GNU Affero General Public License (version 3); see /COPYING.
    Copyright (C) 2023 metamuffin <metamuffin@disroot.org>
*/
pub mod astar;
pub mod data;
pub mod database;
pub mod description;
pub mod import;

use database::Database;
use serde::{Deserialize, Serialize};
use std::{f64::consts::PI, sync::Arc};

#[derive(Debug, Serialize, Deserialize, Clone, Copy, PartialEq, PartialOrd, Default)]
pub struct Location {
    pub lat: i32, // in decimicrodegree
    pub lon: i32, // in decimicrodegree
}

pub struct Pfadfinder {
    pub database: Database,
}

impl Pfadfinder {
    pub fn new(database: Database) -> Arc<Self> {
        Arc::new(Self { database })
    }
}

const CHUNK_SIZE: i32 = 10000; // approx. 100m at the equator

impl Location {
    pub fn distance(a: Self, b: Self) -> f64 {
        6_371_000.0
            * (a.lat_rad().sin() * b.lat_rad().sin()
                + a.lat_rad().cos() * b.lat_rad().cos() * (b.lon_rad() - a.lon_rad()).cos())
            .acos()
    }
    // in radians
    #[inline]
    pub fn lat_rad(&self) -> f64 {
        self.lat as f64 * 1e-7 / 180.0 * PI
    }
    // in radians
    #[inline]
    pub fn lon_rad(&self) -> f64 {
        self.lon as f64 * 1e-7 / 180.0 * PI
    }
    // in degree
    #[inline]
    pub fn lat_deg(&self) -> f64 {
        self.lat as f64 * 1e-7
    }
    // in degree
    #[inline]
    pub fn lon_deg(&self) -> f64 {
        self.lon as f64 * 1e-7
    }

    pub fn from_degree(lat: f64, lon: f64) -> Self {
        Self {
            lat: (lat * 1e7) as i32,
            lon: (lon * 1e7) as i32,
        }
    }

    pub fn chunk(&self) -> (i32, i32) {
        ((self.lat / CHUNK_SIZE), (self.lon / CHUNK_SIZE))
    }
    pub fn direction_to(&self, other: Location) -> f64 {
        let dlat = other.lat_rad() - self.lat_rad();
        // accounting for smaller circumference closer to poles by scaling.
        let dlon = (other.lon_rad() - self.lon_rad()) * self.lat_rad().cos();
        dlon.atan2(dlat) // TODO assumes the earth is flat
    }
}
