# pfadfinder

A route planning engine for openstreetmap. Uses a multithreaded A*-like
algorithm to plan paths, respecting speed limits and oneways.

## usage

1. Install project
   `cargo install --git https://codeberg.org/metamuffin/pfadfinder`
2. Download map source. OpenStreetMap data in Protobuf format is accepted.
   (downloadable from [Geofabrik](https://download.geofabrik.de/))
3. Run `pfadfinder-server -m path/to/map.osm.pbf` (the initial import requires a
   few seconds)

## licence

AGPL-3.0-only: See [`/COPYING`](/COPYING)
